# Hydrophis Login

![License](https://img.shields.io/badge/license-Motosoto-blue.svg) [![Latest Stable Version](https://img.shields.io/packagist/v/ffzhou/hydrophis-login.svg)](https://packagist.org/packages/ffzhou/hydrophis-login)

A [Flarum](http://flarum.org) extension. When logging in to a member account, visit the login interface of Hydrophis

### Installation

Use [Bazaar](https://discuss.flarum.org/d/5151-flagrow-bazaar-the-extension-marketplace) or install manually with composer:

```sh
composer require ffzhou/hydrophis-login
```

### Updating

```sh
composer update ffzhou/hydrophis-login
```

### Links

- [Packagist](https://packagist.org/packages/ffzhou/hydrophis-login)
