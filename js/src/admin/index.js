app.initializers.add('ffzhou/hydrophis-login', () => {
  // console.log('[ffzhou/hydrophis-login] Hello, admin!');
  app.extensionData
    .for('ffzhou-hydrophis-login')
    .registerSetting(
      {
        setting: 'ffzhou-hydrophis-login.login_api',
        label: app.translator.trans('ffzhou-hydrophis-login.admin.login_api'),
        type: 'text',
      },
    )
    .registerSetting(
      {
        setting: 'ffzhou-hydrophis-login.auto_login_api',
        label: app.translator.trans('ffzhou-hydrophis-login.admin.auto_login_api'),
        type: 'text',
      },
    )
    .registerSetting(
      {
        setting: 'ffzhou-hydrophis-login.auto_login_token_attr',
        label: app.translator.trans('ffzhou-hydrophis-login.admin.auto_login_token_attr'),
        type: 'text',
      },
    )
    .registerSetting(
      {
        setting: 'ffzhou-hydrophis-login.mqtt_enable',
        label: app.translator.trans('ffzhou-hydrophis-login.admin.mqtt_enable'),
        type: 'boolean',
      },
    )
    .registerSetting(
      {
        setting: 'ffzhou-hydrophis-login.mqtt_swoole_server',
        label: app.translator.trans('ffzhou-hydrophis-login.admin.mqtt_swoole_server'),
        type: 'text',
      },
    )
    .registerSetting(
      {
        setting: 'ffzhou-hydrophis-login.mqtt_swoole_server_port',
        label: app.translator.trans('ffzhou-hydrophis-login.admin.mqtt_swoole_server_port'),
        type: 'text',
      },
    )
});
