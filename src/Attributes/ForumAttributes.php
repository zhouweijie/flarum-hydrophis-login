<?php


namespace Ffzhou\HydrophisLogin\Attributes;


use Flarum\Api\Serializer\ForumSerializer;
use Flarum\Settings\SettingsRepositoryInterface;
use Psr\Http\Message\ServerRequestInterface;

class ForumAttributes
{

    /**
     * @var SettingsRepositoryInterface
     */
    protected $settings;
    /**
     * @var ServerRequestInterface
     */
    protected $request;

    public function __construct(SettingsRepositoryInterface $settings)
    {
        $this->settings = $settings;
        $this->request = $request;
    }

    public function __invoke(ForumSerializer $serializer)
    {
        // 判断是否隐藏登出按钮
        // 获取自动登陆token属性
        $tokenAttr = $this->settings->get('ffzhou-hydrophis-login.auto_login_token_attr');
        // 判断请求头中是否包含自动登陆token
        if ($tokenAttr && $serializer->getRequest()->getHeader($tokenAttr)) {
            $isHideLogOut = true;
        }
        else {
            $isHideLogOut = false;
        }

        $attributes = [
            'isHideLogOut' => $isHideLogOut,
        ];
        return $attributes;
    }
}
