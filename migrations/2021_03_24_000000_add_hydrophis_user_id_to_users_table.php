<?php

/*
 * This file is part of fof/links.
 *
 * Copyright (c) 2019 - 2021 FriendsOfFlarum.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\Builder;

return [
    'up' => function (Builder $schema) {
        /*if ($schema->hasColumn('users', 'hydrophis_user_id')) {
            return;
        }
        $schema->table('users', function (Blueprint $table) {
            $table->integer('hydrophis_user_id', false, true)->default(0);
        });*/
    },

    'down' => function (Builder $schema) {
        /*$schema->table('users', function (Blueprint $table) {
            $table->dropColumn('hydrophis_user_id');
        });*/
        $schema->getConnection()->table('settings')
            ->where('key', 'LIKE', 'ffzhou-hydrophis-login.%')
            ->delete();
    },
];
