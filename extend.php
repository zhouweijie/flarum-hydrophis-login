<?php

/*
 * This file is part of ffzhou/hydrophis-login.
 *
 * Copyright (c) 2021 Zhouweijie &lt;1309920680@qq.com&gt;.
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace Ffzhou\HydrophisLogin;

use Ffzhou\HydrophisLogin\Api\Controller\ChangeUsernameController;
use Ffzhou\HydrophisLogin\Attributes\ForumAttributes;
use Ffzhou\HydrophisLogin\Listener\ActivateUser;
use Ffzhou\HydrophisLogin\Listener\PostedListen;
use Ffzhou\HydrophisLogin\Middleware\LoginMiddleware;
use Flarum\Api\Serializer\ForumSerializer;
use Flarum\Extend;
use Flarum\Post\Event\Posted;
use Flarum\User\Event\Registered;

return [
    (new Extend\Frontend('forum'))
        ->js(__DIR__.'/js/dist/forum.js')
        ->css(__DIR__.'/resources/less/forum.less'),
    (new Extend\Frontend('admin'))
        ->js(__DIR__.'/js/dist/admin.js')
        ->css(__DIR__.'/resources/less/admin.less'),
    new Extend\Locales(__DIR__ . '/resources/locale'),
    // 注册中间件
    (new Extend\Middleware('forum'))
        ->add(LoginMiddleware::class),
    // 注册配置项
    (new Extend\ApiSerializer(ForumSerializer::class))
        ->attributes(ForumAttributes::class),
    // 监听确认邮箱事件
    (new Extend\Event())
        ->listen(Registered::class, [ActivateUser::class, 'activateUser']),
    // 注册路由
    (new Extend\Routes('api'))
        ->post('/hydrophis-username-change', 'hydrophis.username.change', ChangeUsernameController::class),
    // 监听新回复
    (new Extend\Event())
        ->listen(Posted::class, [PostedListen::class, 'handle']),

];
